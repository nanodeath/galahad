import java.io.InputStream
import java.io.Writer
import java.lang.reflect.Method
import java.net._   
import java.util.concurrent.Executors
import com.sun.net.httpserver._
import scala.util.matching.Regex
import scala.collection.mutable

package nanodeath.galahad {
    trait Matcher {
        def matches(exchange: HttpExchange): Boolean
    }
     
    class ExactMatcher(path: String, ignoreQuery: Boolean) extends Matcher {

        def this(path: String) = this(path, false)
        override def matches(exchange: HttpExchange) = exchange.getRequestURI.toString == path
    }


    object MethodMatcher {
		val MatchedMethodAttribute = "matched_method"
	}

    class MethodMatcher(arg_path: String, app: Application) extends Matcher {
        private lazy val methods = Map.empty[String, Method] ++ app.getClass.getMethods.map(m => m.getName -> m)
        private val index_method_name = "index"

		private val routeRegex = """(.+)Application""".r
		private val path = 
			if(arg_path == ""){
				app.getClass.getSimpleName match {
					case routeRegex(name) =>
						"/" + name.toLowerCase()
					case _ =>
						throw new RuntimeException("Not sure where to mount app.  Name your app [foo]Application or specify a baseUrl")
				}
			} else {
				arg_path
			}

        override def matches(exchange: HttpExchange) = {
            var uri = exchange.getRequestURI.toString   
            if(uri.startsWith(path)){
				val l = if(path.length < uri.length) path.length + 1 else path.length
                val expected_method = uri.drop(l).split("/")
                if(expected_method(0) == "" && methods.contains(index_method_name)) {
                    exchange.setAttribute(MethodMatcher.MatchedMethodAttribute, methods(index_method_name))
                    true
                } else if(methods.contains(expected_method(0))){
                    exchange.setAttribute(MethodMatcher.MatchedMethodAttribute, methods(expected_method(0)))
                    true
                } else {
                    false
                }
            } else {
                false
            }
        }
    }

	trait MatcherApplication extends Application {
		val baseUrl = ""
		val autoRegister = true
	}

	trait MethodMatcherApplication extends MatcherApplication {
		if(autoRegister){
			Routes.register(new MethodMatcher(baseUrl, this), this)
		}

		override def process(req: Request) = {
			req.exchange.getAttribute(MethodMatcher.MatchedMethodAttribute).toString
			val m: Method = req.exchange.getAttribute(MethodMatcher.MatchedMethodAttribute).asInstanceOf[Method]
			val result = parameterMode.dispatchWithParameters(m, this, req)
			if(result.isInstanceOf[String]){
				Some[String](result.asInstanceOf[String])
			} else {
				None
			}
		}
	}
}
