package nanodeath.galahad {
    object ResponseCode extends Enumeration {
        type ResponseCode = Value
        val OK = Value("200")
        
        val NotFound = Value("404")
		
		val ServerError = Value("500")
    }
}