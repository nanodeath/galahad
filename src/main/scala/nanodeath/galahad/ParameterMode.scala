import java.lang.reflect.Method
import scala.collection.mutable

package nanodeath.galahad {
	abstract class ParameterMode {
		def dispatchWithParameters(m: Method, a: Application, r: Request): Any
	}
	
	// i.e. /test/mrar?param1=foo&param2=bar -> Test.mrar(foo, bar)
	object QueryParameters extends ParameterMode {
		def dispatchWithParameters(m: Method, a: Application, r: Request) = {
			// TODO read in parameters from URL
		}
	}

	// i.e. /test/mrar/1-Red/2-Blue -> Test.mrar(1-Red, 2-Blue)
	object URLParameters extends ParameterMode {
		def dispatchWithParameters(m: Method, a: Application, r: Request) = {
			// TODO
		}
	}
	
	// i.e. /test/mrar?foo=bar -> Test.mrar() w/ params object
	object BasicParameters extends ParameterMode {
		def dispatchWithParameters(m: Method, a: Application, r: Request) = {
			val params: mutable.Map[String, List[String]] = mutable.Map.empty[String, List[String]]
			println(r.exchange.getRequestURI)
			a.params = Map.empty[String, List[String]] ++ params
			m.invoke(a)
		}
	}
}