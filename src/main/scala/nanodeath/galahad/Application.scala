import org.fusesource.scalate._
import org.slf4j.{Logger, LoggerFactory}

package nanodeath.galahad {
    abstract class Application {
        def process(req: Request): Option[String]

		final def new_instance = this.getClass.getConstructor().newInstance().asInstanceOf[Application]

		implicit def stringToOption(s: String) = Some(s)

		lazy val Logger = LoggerFactory.getLogger(this.getClass)

		// Automatic, set by framework
		var params: Map[String, List[String]] = null
        var context: RenderContext = null
        
        // Manual, set by dev (optionally)
		var template: String = ""

		val parameterMode: ParameterMode = BasicParameters
    }
}