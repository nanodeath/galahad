import com.sun.net.httpserver._

package nanodeath.galahad {
    class Handler extends HttpHandler {
        import java.io._ 
        import java.nio._
        
        def handle(exchange: HttpExchange) {
            val startTime = System.nanoTime
            val writer = new BufferedWriter(new OutputStreamWriter(exchange.getResponseBody()))
            try {
                Routes.dispatch(exchange, writer)
            } catch {
                case e => 
					e.printStackTrace
                	throw e
            } finally {
                writer.flush()  
                exchange.close()
                println("Completed request in " + (System.nanoTime - startTime) / 1000000000.0 + " seconds.")
            }

        }
    }
}