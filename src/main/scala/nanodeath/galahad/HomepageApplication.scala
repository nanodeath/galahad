package nanodeath.galahad {
    class HomepageApplication extends Application {
        override def process(req: Request) = {
            "homepage!"
        }
    }
}