import java.net._
import java.util.concurrent.Executors
import com.sun.net.httpserver._


package nanodeath.galahad {
	object Server {
		implicit def convertScalaListToJavaList(aList:List[Filter]) = java.util.Arrays.asList(aList.toArray: _*)
		val port = 6666

		def main(args: Array[String]) {
			val server = HttpServer.create(new InetSocketAddress(port), 10)
			
			// Here we're manually registering an app with a URL
			Routes.register(new ExactMatcher("/"), new HomepageApplication)
			
			// Here we're instructing the application to load itself with the router
			TestApplication.load

			// Instantiate our main context and pass in our new handler object
			val c: HttpContext = server.createContext("/", new Handler())
			
			
			c.getFilters().add(new LoggingFilter)
			println(c.getFilters())

			server.setExecutor(Executors.newCachedThreadPool());
			Console.println("Starting up...")
			Console.println("Server listening on port %d" format port)
			server.start
			while(Console.readLine() == null) {}
			Console.println("Shutting down...")
			server.stop(0)
		}
	}
}