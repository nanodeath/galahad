import java.io.Writer
import com.sun.net.httpserver._

package nanodeath.galahad {
    class Request(
            method: String,
            reqHeaders: Map[String, List[String]],
            resHeaders: Map[String, List[String]],
            bodyWriter: Writer,
            uri: String,
            ex: HttpExchange
        ) {
        val exchange = ex
        val writer = bodyWriter
		lazy val parameters = {
			var uri = exchange.getRequestURI
		}
        def this(ex: HttpExchange, w: Writer) = this(ex.getRequestMethod, null, null, w, ex.getRequestURI.toString, ex)
    }
}