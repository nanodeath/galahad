package nanodeath.galahad {
    class FourOhFourApplication extends Application {
        override def process(req: Request) = {
            "404!"
        }
    }
}