import com.sun.net.httpserver._
import java.io.{Writer, PrintWriter, StringWriter}
import java.util.concurrent.Executors
import scala.collection.mutable
import org.fusesource.scalate._

package nanodeath.galahad {
    object Routes {
        private var route_map = mutable.Map.empty[Matcher, Application]
        private var defaultApp: Application = new FourOhFourApplication
     
        def register(m: Matcher, a: Application) {
            route_map(m) = a 
        }

        def register_default(a: Application) {
            defaultApp = a
        }

		val engine = new TemplateEngine

        def dispatch(exchange: HttpExchange, writer: Writer) { 
            val request = new Request(exchange, writer)
            // optimize listification, with something like lazy?
            val a = route_map.toList.find((pair: (Matcher, Application)) => pair._1.matches(exchange)) match {
                case Some((_, app)) => 
                    app.new_instance
                case _ =>
                    defaultApp
            }
			
			// TODO it'd be nice if we could make this lazy somehow, in case there's no template
            a.context = new DefaultRenderContext(engine, new PrintWriter(writer))

			var processFailed = false
			val result: Option[String] = try {
				a.process(request)
			} catch {
				case e => 
					a.Logger.error("{}", e.getStackTrace.mkString("|"))
					exchange.sendResponseHeaders(ResponseCode.ServerError.toString.toInt, 0)
					processFailed = true
					None
					
			}
			if(!processFailed){
                if(a.template != "") {
					
                    var template: Template = null
                    try {
						a.Logger.info("Template loading and rendering.")
                        template = engine.load(a.template)
                        template.render(a.context)
                    } catch {
                        case e => 
                        	processFailed = true
							e.printStackTrace
                    }
                } else {
                	a.Logger.info("No template provided, writing {}", result.getOrElse(""))
                    writer.write(result.getOrElse(""))
                }
                if(processFailed){
					exchange.sendResponseHeaders(ResponseCode.ServerError.toString.toInt, 0)
				} else {
					exchange.sendResponseHeaders(ResponseCode.OK.toString.toInt, 0)
				}
			} else {
				a.Logger.info("Process failed.")
				writer.write("fail")
			}
        }
    }
}