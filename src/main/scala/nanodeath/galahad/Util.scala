package nanodeath.galahad {
	object Util {
		import scala.actors._
		import scala.actors.Actor._

        def actorify[T, R](f: T => R) = {
            actor {
                Actor.loop {
                    react {
                        case e: T => reply(f(e): R)
                        case _ =>
                    }
                }
            }
        }
	}
}