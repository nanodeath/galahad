import com.sun.net.httpserver._

package nanodeath.galahad {
    class LoggingFilter extends Filter {
        def description = "I'm a filter!"

        def doFilter(exchange: HttpExchange, chain: Filter.Chain) {
			println("Request received: " + exchange.getRequestURI)
            chain.doFilter(exchange)
        }
    }
}