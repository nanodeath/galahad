import java.lang.reflect.Method
import org.fusesource.scalate._
import java.io.{PrintWriter, StringWriter}
import scala.actors._
import scala.actors.Actor._
import scala.xml._

package nanodeath.galahad {
	object TestApplication {
		lazy val instance = new TestApplication

		def load {
			instance
		}
        val engine = new TemplateEngine
	}
	
    class TestApplication extends Application with MethodMatcherApplication {
    	import TestApplication.engine

        def index = {
            "Test index!"
        }

        def red = {
            "Red!"
        }

		def show_template = {
			template = "templates/foo.ssp"
			context.attributes("weather") = "balmy"
		}

		def weather = {
			val weatherFuture = weatherFetcher !! "http://www.weather.gov/xml/current_obs/KBFI.xml"
			val weather = (XML.loadString(weatherFuture().asInstanceOf[String]) \ "weather").text
			"The weather is " + weather.toLowerCase + "!"
		}
		
		private def downloadURL(url: String) = {
			import java.net._
			import java.io._
			
			val properURL = new URL(url)
			val reader = new BufferedReader(new InputStreamReader(properURL.openStream()))
			val output = new StringBuilder
			var inputLine = ""
			while(inputLine != null) {
				inputLine = reader.readLine()
				if(inputLine != null) output.append(inputLine)
			}
			
			reader.close
			output.toString
		}
		
		val weatherFetcher = Util.actorify(downloadURL _)
    }
}
